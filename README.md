# antonyms_detection
This repos contains testing datasets (AntSyn1000.txt, AntHyp1000.txt, AntMix1000.txt), and training datasets (ants_training_data.txt, non_ants_training_data.txt) which can be splitted further into train/dev datasets. All samples in Testing datasets are not included in training datasets.

These datasets are created based on:
+ Vicon (Nguyen et al., 2018)
+ Vds1, Vds2, Vds3 (Tan et al., 2018)
+ Vietnamese WordNet (Phuong  Thai  Nguyen,  Van  Lam  Pham,  Hoang  Anh  Nguyen,Huy  Hien  Vu,  Ngoc  Anh  Tran,  and  Truong  Thi  Thu  Ha.   Atwo-phase  approach  for  building  vietnamese  wordnet.the  8thGlobal Wordnet Conference, pages 259 – 264, 2015.)
+ Viet-namese  Computational  Lexicon (Nguyen Thi Minh Huyen, Laurent Romary, Mathias Rossignol,and  Xuˆan  Luong  Vu.A  lexicon  for  vietnamese  languageprocessing.Language Resources and Evaluation, 40(3-4):291–309, 2006)

Reference:

[1] Bui Van Tan, Nguyen Phuong Thai and Pham Van Lam. Hypernymy Detection
for Vietnamese Using Dynamic Weighting Neural Network. 19th International
Conference on Computational Linguistics and Intelligent Text Processing, 2018.

[2] Kim Anh Nguyen, Sabine Schulte im Walde, and
Ngoc Thang Vu. Introducing two vietnamese datasets
for evaluating semantic models of (dis-)similarity
and relatedness. In Marilyn A. Walker, Heng Ji,
and Amanda Stent, editors, NAACL-HLT (2), pages
199–205. Association for Computational Linguistics, 2018. ISBN 978-1-948087-29-2.

[3] Hoang-Anh Nguyen Huy-Hien Vu Ngoc-Anh Tran
Thi-Thu Ha Truong Phuong-Thai Nguyen, Van-
Lam Pham. A two-phase approach for building viet-
namese wordnet. the 8th Global Wordnet Conference,
pages 259 – 264, 2015.

Citing.

